# Abertay Attribute Survey System
Version: 1.0

### What is this repository for? ###
A web application which allows admins to create surveys which can be run multiple times throughout the academic year. This allows admins to compare changes of opinion in survey respondents over a period.

### Features ###
Accounts

- Users sign-in to the application using their university account (no account creation required).


Surveys

- Admins can create and manage surveys (utilises Vue.js to simplify the form into a one-page process).
- Admins can duplicate surveys.


Survey Instances

- Admins can create survey instances (determines the active period of a survey)


Responses

- Users can complete surveys to submit their responses.
- Admins can download an Excel document that holds the survey results. Results can be filtered by respondent's school.


Misc

- Responsive and cross browser compatible  design so that the web application is usable across major devices and browsers. 

### Who do I talk to? ###

Software Developer - Grant Hynd <grant.hynd@gmail.com>