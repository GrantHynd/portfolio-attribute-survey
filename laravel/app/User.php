<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * A user represents a staff member or student of Abertay University. 
 */
class User extends Authenticatable
{
    use Notifiable;

    public $timestamps = false;
    protected $fillable = [
        /**
         * Sensitive Information: Redacted for security purposes
         * 
         */
    ];

    /**
     * Get format for dates.
     * 
     * Note: .u is required for live server but it produces a error on local server. 
     * 
     * TODO Find solution that works on both.
     *
     * @return string
     */
    public function getDateFormat() {
        return 'Y-m-d H:i:s.u';
    }

    /**
     * Get the DateTime in the format for MS SQL.
     *
     * @return string
     */
    public function fromDateTime($value) {
        return substr(parent::fromDateTime($value), 0, -3);
    }

    /**
     * Is the user an admin.
     *
     * @return boolean
     */
    public function isAdmin(){
        $isAdmin = false;
        if($this->permission_type == 'admin') {
            $isAdmin = true;
        }
        return $isAdmin;
    }

    /**
     * Has the user completed the specified instance.
     *
     * @param int $instanceId
     * @return boolean
     */
    public function hasCompletedInstance($instanceId) {
        $hasCompletedInstance = false;
        if($this->responses()->where('instance_id', '=', $instanceId)->first() != null) {
            $hasCompletedInstance = true;
        } 
        return $hasCompletedInstance;
    }

    /**
     * Each admin can be the creator of many surveys.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function surveys() {
        return $this->hasMany(Survey::class);
    }

    /**
     * Each user can respond to many survey instances.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsHasMany
     */
    public function responses() {
        return $this->belongsToMany(Instance::class, 'responses', 'user_id', 'instance_id')
            ->withPivot('question_id', 'answer')
            ->withTimestamps()
            ->using(ResponsePivot::class);
    }
}
