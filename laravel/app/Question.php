<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Iatstuti\Database\Support\CascadeSoftDeletes;

/**
 * A question within a section of a survey.
 */
class Question extends Model
{
    use SoftDeletes, CascadeSoftDeletes;
    
    public $timestamps = false;
    protected $fillable = ['section_id', 'type_id', 'text', 'code'];
    protected $cascadeDeletes = ['options'];
    protected $dates = ['deleted_at'];

    /**
     * Get format for dates.
     * 
     * Note: .u is required for live server but it produces a error on local server. 
     * 
     * TODO Find solution that works on both.
     *
     * @return string
     */
    public function getDateFormat() {
        return 'Y-m-d H:i:s.u';
    }

    /**
     * Get the DateTime in the format for MS SQL.
     *
     * @return string
     */
    public function fromDateTime($value) {
        return substr(parent::fromDateTime($value), 0, -3);
    }

    /**
     * Each question is held within a section.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function section() {
        return $this->belongsTo(Section::class);
    }

    /**
     * Each question is of a certain type.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type() {
        return $this->belongsTo(QuestionType::class);
    }

    /**
     * Each question has many options.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function options() {
        return $this->hasMany(QuestionOption::class);
    }

    /**
     * Each question has can be answered (by users) for a particular instance of a survey.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function instances() {
        return $this->belongsToMany(Instance::class, 'responses', 'question_id', 'instance_id')
            ->withPivot('user_id', 'answer')
            ->withTimestamps()
            ->using(ResponsePivot::class);
    }
}
