<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Iatstuti\Database\Support\CascadeSoftDeletes;
use Carbon\Carbon;

/**
 * An instance represents a version of a survey as a survey can be run multiple times.
 */
class Instance extends Model
{
    use SoftDeletes, CascadeSoftDeletes;
    
    protected $fillable = ['user_id', 'survey_id', 'start_at', 'end_at', 'total_responses'];
    protected $cascadeDeletes = ['questions'];
    protected $dates = ['created_at','updated_at','start_at','end_at','deleted_at'];
    protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * Get format for dates.
     * 
     * Note: .u is required for live server but it produces a error on local server. 
     * 
     * TODO Find solution that works on both.
     *
     * @return string
     */
    public function getDateFormat() {
        return 'Y-m-d H:i:s';
    }

    /**
     * Get the DateTime in the format for MS SQL.
     *
     * @return string
     */
    public function fromDateTime($value) {
        return substr(parent::fromDateTime($value), 0, -3);
    }

    /**
     * Fetch instances which are currently running.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('start_at', '<=', Carbon::now())->where('end_at', '>=', Carbon::now());
    }

    /**
     * Fetch instances which have ended.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopeEnded($query)
    {
        return $query->where('end_at', '<=', Carbon::now());
    }

    /**
     * Fetch instances which are scheduled to run in the future.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopeScheduled($query)
    {
        return $query->where('start_at', '>=', Carbon::now());
    }

    /**
     * Is the instance currently active.
     *
     * @return boolean
     */
    public function isActive() {
        $isActive = false;
        if($this->start_at <= Carbon::now() && $this->end_at >= Carbon::now()) {
            $isActive = true; 
        }
        return $isActive;
    }

    /**
     * Each instance is created by a user.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator() {
        return $this->belongsTo(User::class);
    }

    /**
     * Each instance references a survey.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function survey() {
        return $this->belongsTo(Survey::class);
    }

    /**
     * Each instance has many responses (by users) to it's survey questions.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function questions() {
        return $this->belongsToMany(Question::class, 'responses', 'instance_id', 'question_id')
            ->withPivot('user_id', 'answer')
            ->withTimestamps()
            ->using(ResponsePivot::class);
    }
}
