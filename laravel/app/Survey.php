<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Iatstuti\Database\Support\CascadeSoftDeletes;
use Carbon\Carbon;

/**
 * A survey is a collection of questions which are separated into sections. A survey can have multiple occurrences (instances). 
 */
class Survey extends Model
{
    use SoftDeletes, CascadeSoftDeletes;

    protected $fillable = ['user_id', 'name', 'desc'];
    protected $cascadeDeletes = ['sections', 'instances'];
    protected $dates = ['created_at','updated_at','deleted_at'];
    protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * Each survey is created by a creator.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator() {
        return $this->belongsTo(User::class);
    }

    /**
     * Each survey contains many sections.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sections() {
        return $this->hasMany(Section::class);
    }

    /**
     * Each survey contains many questions which are contained in sections.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function questions() {
        return $this->hasManyThrough(Question::class, Section::class);
    }

    /**
     * Each survey has many instances.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function instances() {
        return $this->hasMany(Instance::class);
    }
}
