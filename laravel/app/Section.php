<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Iatstuti\Database\Support\CascadeSoftDeletes;

/**
 * A section contains a block of questions in a survey. The survey can contain multiple sections. 
 */
class Section extends Model
{
    use SoftDeletes, CascadeSoftDeletes;

    public $timestamps = false;
    protected $fillable = ['survey_id', 'name', 'desc'];
    protected $cascadeDeletes = ['questions'];
    protected $dates = ['deleted_at'];

    /**
     * Get format for dates.
     * 
     * Note: .u is required for live server but it produces a error on local server. 
     * 
     * TODO Find solution that works on both.
     *
     * @return string
     */
    public function getDateFormat() {
        return 'Y-m-d H:i:s.u';
    }

    /**
     * Get the DateTime in the format for MS SQL.
     *
     * @return string
     */
    public function fromDateTime($value) {
        return substr(parent::fromDateTime($value), 0, -3);
    }
    
    /**
     * Each section belongs to a survey.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function survey() {
        return $this->belongsTo(Survey::class);
    }

    /**
     * Each section contains many questions.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questions() {
        return $this->hasMany(Question::class);
    }
}
