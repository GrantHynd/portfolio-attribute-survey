<?php 
namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * ResponsePivot represents a pivot table where all responses to survey instances (by users) are stored.
 */
class ResponsePivot extends Pivot {
   protected $dates = ['created_at', 'updated_at', 'deleted_at'];
   protected $dateFormat = 'Y-m-d H:i:s';
}