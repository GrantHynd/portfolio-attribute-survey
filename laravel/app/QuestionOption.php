<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * A question option is a option of a question that users can select as an answer.
 */
class QuestionOption extends Model
{
    use SoftDeletes;
    
    public $timestamps = false;
    protected $fillable = ['question_id', 'text'];
    protected $dates = ['deleted_at'];

    /**
     * Get format for dates.
     * 
     * Note: .u is required for live server but it produces a error on local server. 
     * 
     * TODO Find solution that works on both.
     *
     * @return string
     */
    public function getDateFormat() {
        return 'Y-m-d H:i:s.u';
    }

    /**
     * Get the DateTime in the format for MS SQL.
     *
     * @return string
     */
    public function fromDateTime($value) {
        return substr(parent::fromDateTime($value), 0, -3);
    }

    /**
     * Each question option belongs to a question.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function question() {
        return $this->belongsTo(Question::class);
    }
}
