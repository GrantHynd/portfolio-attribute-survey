<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Home page of website.
     *
     * @return Illuminate\View\View
     */
    public function home() {
        return view('pages.home');
    }
}
