<?php

namespace App\Http\Controllers;

use App\Http\Requests\ResponseRequest;
use App\Survey;
use App\Instance;
use App\User;
use Excel;

class ResponseController extends Controller
{
    /**
     * Store response in database. 
     * 
     * Note: $question['answers'] are held differently for different question types.
     *
     * @param Survey $survey
     * @param Instance $instance
     * @param ResponseRequest $request
     * @return Illuminate\Routing\UrlGenerator
     */
    public function store(Survey $survey, Instance $instance, ResponseRequest $request) {
        foreach($request->input('sections') as $section) {
            foreach($section['questions'] as $question) {
                // Dropdown single select
                if($question['type_id'] == 1) {
                    $answer = $question['answers']['id'];
                    $instance->questions()->attach($question['id'], ['user_id' => $request->user()->id, 'answer' => $answer]); 
                }
                // Dropdown multiselect
                else if($question['type_id'] == 2) {
                    foreach($question['answers'] as $questionAnswer) {
                        $answer = $questionAnswer['id'];
                        $instance->questions()->attach($question['id'], ['user_id' => $request->user()->id, 'answer' => $answer]); 
                    }
                } 
                // Scale
                else if($question['type_id'] == 3) {
                    $answer = $question['answers'];
                    $instance->questions()->attach($question['id'], ['user_id' => $request->user()->id, 'answer' => $answer]); 
                }
            }
        }
        return url('/surveys/'.$survey->id.'/instances/'.$instance->id);
    }

    /**
     * Download excel file of survey instance responses. Can be filtered by school.
     *
     * @param Survey $survey
     * @param Instance $instance
     * @param string|null $school
     * @return void | Illuminate\Contracts\Routing\ResponseFactory
     */
    public function download(Survey $survey, Instance $instance, $school = null) {
        $questions = $instance->questions()->get();
        $excelFormattedResponses = $this->filterResponsesBySchool($questions, $school);

        if($excelFormattedResponses != []) {
            $fileName = 'Survey'.$survey->id.'_Instance'.$instance->id;
            $this->exportToExcel($fileName, $excelFormattedResponses);
        } else {
            return response()->json('No responses available', 422);
        }
    }

    /**
     * Check if the respondent's school matches the specified school.
     *
     * @param Illuminate\Database\Eloquent\Collection $questions
     * @param string|null $school
     * @return array
     */
    public function filterResponsesBySchool($questions, $school) {
        $excelFormattedResponses = [];
        $questionIndex = 1;
        foreach ($questions as $responseIndex => $question) {
            $currentRespondent = User::find($question->pivot->user_id);

            // If question response has the same id as previous question response then decrement the question index. 
            if($responseIndex != 0 && $question->id == $questions[$responseIndex-1]->id) {
                $questionIndex--;
            }

            // If the user id changes then reset the question index to beginning of survey.
            if($responseIndex != 0 && $currentRespondent->id != $questions[$responseIndex-1]->pivot->user_id) {
                $questionIndex = 1;
            }

            switch($school) {
                case(null):
                    $excelFormattedResponses[$responseIndex] = $this->formatResponseForExcel($questionIndex, $question, $currentRespondent);
                    break;
                case('DBS'):
                case('SET'):
                case('SHS'):
                    if($currentRespondent->school_code === $school) {
                        $excelFormattedResponses[$responseIndex] = $this->formatResponseForExcel($questionIndex, $question, $currentRespondent);
                    }
                    break;
                case('SDI'):
                    //  Note: Compatibility until AMG is updated to DSI in December!
                    if($currentRespondent->school_code === 'AMG' || $currentRespondent->school_code === $school) {
                        $excelFormattedResponses[$responseIndex] = $this->formatResponseForExcel($questionIndex, $question, $currentRespondent);
                    }
                    break;
                case('graduate-school'):
                    if($currentRespondent->employeetype === 'Researcher') {
                        $excelFormattedResponses[$responseIndex] = $this->formatResponseForExcel($questionIndex, $question, $currentRespondent);
                    }
                    break;
            }
            $questionIndex++;
        }
        return $excelFormattedResponses;
    }

    /**
     * Format a response for displaying in Excel.
     *
     * @param Int $questionIndex
     * @param Illuminate\Database\Eloquent\Model $question
     * @param Illuminate\Database\Eloquent\Model $currentRespondent
     * @return array
     */
    public function formatResponseForExcel($questionIndex, $question, $currentRespondent) {
        $response = [];
        // If the answer references an option then fetch the option text.
        $answer = null;
        if($question->type->type == 'select' || $question->type->type == 'multiselect') {
            $questionOptions = $question->options()->where('question_id', '=', $question->id)->get();
            foreach($questionOptions as $key => $option) {
                if($option['id'] == $question->pivot->answer) {
                    $answer = $option['text'];
                    $answerKey = $key+1;
                } 
            }
        } 
        elseif($question->type->type == '7_point_scale') {
            $answer = $question->pivot->answer;
            $answerKey = (int) $question->pivot->answer;
        }

        $response['user_id'] = $question->pivot->user_id;
        $response['school'] = $currentRespondent->school_code;
        $response['course_code'] = $currentRespondent->course_code;
        $response['question_id'] = $questionIndex;
        $response['type'] = $question->type->type;
        $response['code'] = $question->code;
        $response['question'] = $question->text;
        $response['answer'] = $answer;
        $response['answer_key'] = $answerKey;
        $response['submitted_at'] = $question->pivot->created_at->toDateTimeString();

        return $response;
    }

    /**
     * Export survey responses to Excel file.
     *
     * @param string $fileName
     * @param array $excelFormattedResponses
     * @return Excel
     */
    public function exportToExcel($fileName, $excelFormattedResponses) {
        return Excel::create($fileName, function($excel) use ($excelFormattedResponses) {
			$excel->sheet('SurveyResponses', function($sheet) use ($excelFormattedResponses)
	        {
				$sheet->fromArray($excelFormattedResponses);
	        });
		})->download('xls');
    }
}
