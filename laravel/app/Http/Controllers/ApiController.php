<?php

namespace App\Http\Controllers;
use App\QuestionType;
use App\Survey;
use App\Section;
use App\Instance;

class ApiController extends Controller
{
    /**
     * Get all the question types.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function getQuestionTypes() {
        return QuestionType::all();
    }

    /**
     * Get the relevant survey instance 
     *
     * @param Survey $survey
     * @param Instance $instance
     * @return Illuminate\Database\Eloquent\Model
     */
    public function getSurveyInstance(Survey $survey, Instance $instance) {
        $survey['instance_start_at'] = $instance->start_at->toDateString();
        $survey['instance_end_at'] = $instance->end_at->toDateString();
        return $survey;
    }

    /**
     * Get survey data including sections, questions and question options.
     *
     * @param Survey $survey
     * @return Illuminate\Database\Eloquent\Model
     */
    public function getSurvey(Survey $survey) {
        $sections = [];

        $i = 0;
        foreach($survey->sections()->get() as $section) {
            $questions = $section->questions()->get();
            $sections[$i]['id']        = $section->id;
            $sections[$i]['survey_id'] = $section->survey_id;
            $sections[$i]['name']      = $section->name;
            $sections[$i]['desc']      = $section->desc;
            $sections[$i]['questions'] = $questions;

            $j = 0;
            foreach($section->questions()->get() as $question) {
                $options = $question->options()->get();
                $sections[$i]['questions'][$j]['options'] = $options;
                $j++;
            }
            $i++;
        }
        $survey['sections'] = $sections;
        return $survey;
    }

    /**
     * Get a single section (including its questions and question options) in a paginated format.
     *
     * @param Survey $survey
     * @return Illuminate\Pagination\LengthAwarePaginator
     */
    public function getSections(Survey $survey) {
        $sections = $survey->sections()->paginate('1');

        $i = 0;
        foreach($sections as $section) {
            $questions = $section->questions()->get();
            $sections[$i]['id']        = $section->id;
            $sections[$i]['survey_id'] = $section->survey_id;
            $sections[$i]['name']      = $section->name;
            $sections[$i]['desc']      = $section->desc;
            $sections[$i]['questions'] = $questions;
            
            $j = 0;
            foreach($section->questions()->get() as $question) {
                $options = $question->options()->get();
                $sections[$i]['questions'][$j]['options'] = $options;
                $sections[$i]['questions'][$j]['answers'] = null;
                $sections[$i]['questions'][$j]['errors'] = null;

                $j++;
            }
            $i++;
        }
        return $sections;
    }
}
