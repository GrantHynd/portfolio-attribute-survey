<?php

namespace App\Http\Controllers;

use App\Survey;
use App\Instance;
use App\Section;
use App\QuestionType;
use App\QuestionOption;
use App\Question;
use App\Http\Requests\SurveyRequest;

class SurveyController extends Controller
{
    /**
     * Show a list of all surveys.
     *
     * @return Illuminate\View\View
     */
    public function adminIndex() {
        $surveys = Survey::all();
        return view('administrator.surveys.index', compact('surveys'));
    }
        
    /**
     * Show a individual survey.
     *
     * @param Survey $survey
     * @return Illuminate\View\View
     */
    public function adminShow(Survey $survey) {
        return view('administrator.surveys.show', compact('survey'));
    }

    /**
     * Show a form to create a survey.
     *
     * @return Illuminate\View\View
     */
    public function create() {
        return view('administrator.surveys.create');
    }

    /**
     * Store a new survey in the database. This includes all of the sub-components of a survey (sections, questions, question options)
     *
     * @param SurveyRequest $request
     * @return Illuminate\Routing\UrlGenerator
     */
    public function store(SurveyRequest $request) {  
        $newSurvey = Survey::create([
            'user_id' => $request->user()->id,
            'name' => request('survey.name'),
            'desc' => request('survey.desc')
        ]);

        foreach($request['survey.sections'] as $sectionInput) {
            $newSection = $this->createSection($newSurvey->id, $sectionInput);

            foreach($sectionInput['questions'] as $questionInput) {
                $newQuestion = $this->createQuestion($newSection->id, $questionInput);

                if(isset($questionInput['options'])) {
                    foreach($questionInput['options'] as $option) {
                        $newOption = $this->createOption($newQuestion->id, $option);
                    }
                }
            }
        }
        return url('/administrator/surveys/'.$newSurvey->id);
    }

    /**
     * Show an form for editing a survey.
     *
     * @param Survey $survey
     * @return Illuminate\View\View
     */
    public function edit(Survey $survey) {
        return view('administrator.surveys.edit', compact('survey'));
    }

    /**
     * Update a survey in the database. This includes all of the sub-components of a survey (sections, questions, question options)
     * Sub-components can either be created or updated and are processed differently.
     *
     * @param Survey $survey
     * @param SurveyRequest $request
     * @return Illuminate\Routing\UrlGenerator
     */
    public function update(Survey $survey, SurveyRequest $request) {
        $survey->update([
            'name' => request('survey.name'),
            'desc' => request('survey.desc')
        ]);

        foreach($request['survey.sections'] as $sectionInput) {
            if(!isset($sectionInput['id'])) {
                $newSection = $this->createSection($survey->id, $sectionInput);
                $sectionInput['id'] = $newSection->id;
            } 
            else if(isset($sectionInput['id'])) {
                $this->findAndUpdateSection($sectionInput);
            }

            foreach($sectionInput['questions'] as $questionInput) {
                if(!isset($questionInput['id'])) {
                    $newQuestion = $this->createQuestion($sectionInput['id'], $questionInput);
                    $questionInput['id'] = $newQuestion->id;
                } else {
                    $this->findAndUpdateQuestion($questionInput);
                }

                foreach($questionInput['options'] as $optionInput) {
                    if(!isset($optionInput['id'])) {
                        $newOption = $this->createOption($questionInput['id'], $optionInput);
                    } else {
                        $this->findAndUpdateOption($optionInput);
                    }
                }
            }
        }
        return url('/administrator/surveys/'.$survey->id);
    }

    /**
     * Duplicate a survey by creating a new survey with data of an existing survey. 
     *
     * @param Survey $survey
     * @return Illuminate\Routing\Redirector
     */
    public function duplicate(Survey $survey) {
        $newSurvey = Survey::create([
            'user_id' => $survey->user_id,
            'name' => $survey->name .' Copy',
            'desc' => $survey->desc
        ]);

        foreach($survey->sections()->get() as $section) {
            $newSection = $this->createSection($newSurvey->id, $section);

            foreach($section->questions()->get() as $question) {
                $newQuestion = $this->createQuestion($newSection->id, $question);

                foreach($question->options()->get() as $option) {
                    $newOption = $this->createOption($newQuestion->id, $option);
                }
            }
        }
        return redirect(url('/administrator/surveys/'.$newSurvey->id));
    }

    /**
     * Store section in database.
     *
     * @param int $surveyId
     * @param array $sectionInput
     * @return Section
     */
    public function createSection($surveyId, $sectionInput) {
        return Section::create([
            'survey_id' => $surveyId,
            'name'      => $sectionInput['name'],
            'desc'      => $sectionInput['desc']
        ]);
    }

    /**
     * Store question in database.
     *
     * @param int $sectionId
     * @param array $questionInput
     * @return void
     */
    public function createQuestion($sectionId, $questionInput) {
        return Question::create([
            'section_id' => $sectionId,
            'type_id'    => $questionInput['type_id'],
            'text'       => $questionInput['text'],
            'code'       => $questionInput['code']
        ]);
    }

    /**
     * Store question option in database.
     *
     * @param int $questionId
     * @param array $optionInput
     * @return void
     */
    public function createOption($questionId, $optionInput) {
        return QuestionOption::create([
            'question_id' => $questionId,
            'text'    => $optionInput['text']
        ]);
    }

    /**
     * Find and update section in database.
     *
     * @param array $sectionInput
     * @return void
     */
    public function findAndUpdateSection($sectionInput) {
        $section = Section::find($sectionInput['id']);
        $section->update([
            'name'      => $sectionInput['name'],
            'desc'      => $sectionInput['desc']
        ]);
    }

    /**
     * Find and update question in database.
     *
     * @param int $questionInput
     * @return void
     */
    public function findAndUpdateQuestion($questionInput) {
        $question = Question::find($questionInput['id']);
        $question->update([
            'type_id'    => $questionInput['type_id'],
            'text'       => $questionInput['text'],
            'code'       => $questionInput['code']
        ]);
    }

    /**
     * Find and update question option in database.
     *
     * @param int $optionInput
     * @return void
     */
    public function findAndUpdateOption($optionInput) {
        $option = QuestionOption::find($optionInput['id']);
        $option->update([
            'text'    => $optionInput['text']
        ]);
    }

    /**
     * Delete survey from database.
     *
     * @param Survey $survey
     * @return Illuminate\Routing\UrlGenerator
     */
    public function destroy(Survey $survey) {
        $survey->delete();
        return url('/administrator/surveys');
    }
}
