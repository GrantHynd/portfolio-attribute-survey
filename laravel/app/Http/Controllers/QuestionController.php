<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;

class QuestionController extends Controller
{
    /**
     * Delete question from database
     *
     * @param Question $question
     * @return Illuminate\Contracts\Routing\ResponseFactory
     */
    public function destroy(Question $question) {
        $question->delete();
        return response()->json("success", 200);
    }
}
