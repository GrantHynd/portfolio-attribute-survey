<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Survey;
use App\Section;

class SectionController extends Controller
{
    /**
     * Delete section from database.
     *
     * @param Survey $survey
     * @param Section $section
     * @return Illuminate\Contracts\Routing\ResponseFactory
     */
    public function destroy(Survey $survey, Section $section) {
        $section->delete();
        return response()->json("success", 200);
    }
}
