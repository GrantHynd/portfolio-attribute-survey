<?php

namespace App\Http\Controllers;

use App\Survey;
use App\Instance;
use App\Http\Requests\InstanceRequest;
use Carbon\Carbon;

class InstanceController extends Controller
{
    /**
     * Show a survey instance.
     *
     * @param Survey $survey
     * @param Instance $instance
     * @return Illuminate\View\View
     */
    public function show(Survey $survey, Instance $instance) {
        $numberOfResponses = $instance->questions()->distinct('user_id')->count('user_id');
        return view('surveys.instances.show', compact('survey', 'instance', 'numberOfResponses'));
    }

    /**
     * Store an instance in the database.
     *
     * @param Survey $survey
     * @param InstanceRequest $request
     * @return Illuminate\Routing\Redirector
     */
    public function store(Survey $survey, InstanceRequest $request) {

        $startDate = strtotime(request('start_at'));
        $formattedStartDate = date('Y-m-d', $startDate);

        $endDate = strtotime(request('end_at'));
        $formattedEndDate = date('Y-m-d', $endDate);

         Instance::create([
            'user_id'         => $request->user()->id,
            'survey_id'       => $survey->id,
            'start_at'        => $formattedStartDate,
            'end_at'          => $formattedEndDate,
        ]);

        return redirect(url('/administrator/surveys/' . $survey->id));
    }

    /**
     * Delete instance from database.
     *
     * @param Survey $survey
     * @param Instance $instance
     * @return Illuminate\Routing\UrlGenerator
     */
    public function destroy(Survey $survey, Instance $instance) {
        $instance->delete();
        return url('/administrator/surveys/'.$survey->id);
    }
}
