<?php

namespace App\Http\Controllers;
use App\QuestionOption;

use Illuminate\Http\Request;

class QuestionOptionController extends Controller
{
    /**
     * Delete question option from database
     *
     * @param QuestionOption $option
     * @return Illuminate\Contracts\Routing\ResponseFactory
     */
    public function destroy(QuestionOption $option) {
        $option->delete();
        return response()->json("success", 200);
    }
}
