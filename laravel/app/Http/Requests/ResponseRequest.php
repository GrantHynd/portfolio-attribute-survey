<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResponseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sections' => 'required|array',
            'sections.*.id' => 'required|integer',
            'sections.*.questions.*.id' => 'required|integer',
            'sections.*.questions.*.type_id' => 'required|integer',
            'sections.*.questions.*.answers' => 'required',
        ];
    }

    /**
     * Get the error message for each validation rule.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'sections.required' => 'No sections were completed',
            'sections.*.id.required'  => 'Section could not be found',
            'sections.*.questions.*.id.required'  => 'Question could not be found',
            'sections.*.questions.*.type_id.required'  => 'QuestionType could not be found',
            'sections.*.questions.*.answers.required'  => 'Answer could not be found',
        ];
    }
}
