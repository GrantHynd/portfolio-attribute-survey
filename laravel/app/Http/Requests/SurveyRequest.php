<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SurveyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'survey.name' => 'required',
            "survey.sections.*.name" => 'required',
            "survey.sections.*.questions.*.text" => 'required',
            "survey.sections.*.questions.*.options" => 'required|min:2',
            "survey.sections.*.questions.*.options.*.text" => 'required',
            "survey.sections.*.questions.*.code" => 'required',
        ];
    }

    /**
     * Get the error message for each validation rule.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'survey.name.required' => 'A title is required',
            'survey.sections.*.name.required'  => 'A title is required',
            'survey.sections.*.questions.*.options.min'  => 'At least 2 options are required',
            'survey.sections.*.questions.*.options.required'  => 'At least 2 options are required',
            'survey.sections.*.questions.*.options.*.text.required'  => 'An option can not be blank',
            'survey.sections.*.questions.*.text.required'  => 'A question can not be blank',
            'survey.sections.*.questions.*.code.required'  => 'A code is required',
        ];
    }
}
