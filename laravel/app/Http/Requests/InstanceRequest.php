<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InstanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_at' => 'required',
            'end_at' => 'required',
        ];
    }

    /**
     * Get the error message for each validation rule.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'start_at.required' => 'Start date is required',
            'end_at.required'  => 'End date is required'
        ];
    }
}
