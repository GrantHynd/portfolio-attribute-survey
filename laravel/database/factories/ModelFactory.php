<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    /**
     * Sensitive Information: Redacted for security purposes
     * 
     */
});

$factory->define(App\Survey::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->sentence,
        'desc' => $faker->paragraph,
    ];
});

$factory->define(App\Section::class, function (Faker\Generator $faker) {
    $surveyIds = App\Survey::all()->pluck('id')->toArray();

    return [
        'survey_id' => $faker->randomElement($surveyIds),
        'name'      => $faker->sentence,
        'desc'      => $faker->paragraph,
    ];
});

// TODO add all question types. 
// No fake question types.
$factory->define(App\QuestionType::class, function (Faker\Generator $faker) {
    return [
        'type' => '7 point scale',
    ];
});

$factory->define(App\Question::class, function (Faker\Generator $faker) {
    $sectionIds = App\Section::all()->pluck('id')->toArray();
    $typeIds = App\QuestionType::all()->pluck('id')->toArray();

    return [
        'section_id' => $faker->randomElement($sectionIds),
        'type_id'    => $faker->randomElement($typeIds),
        'text'       => $faker->sentence,
        'code'       => 'personal'
    ];
});

$factory->define(App\QuestionOption::class, function (Faker\Generator $faker) {
    $questionIds = App\Question::all()->pluck('id')->toArray();

    return [
        'question_id' => $faker->randomElement($questionIds),
        'text'        => $faker->sentence,
    ];
});

$factory->define(App\Instance::class, function (Faker\Generator $faker) {
    $surveyIds = App\Survey::all()->pluck('id')->toArray();

    return [
        'user_id'         => 1,
        'survey_id'       => $faker->randomElement($surveyIds),
        'start_at'        => $faker->dateTimeThisMonth($max = 'now', $timezone = date_default_timezone_get()),
        'end_at'          => $faker->dateTimeBetween($startDate = 'now', $endDate = '11 months', $timezone = date_default_timezone_get()),
    ];
});