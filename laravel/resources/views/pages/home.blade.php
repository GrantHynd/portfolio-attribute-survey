@extends('layout')

@section('content')
<div id="front">
	<header id="front-hero" role="banner" style="background-image: url({{ url('/images/featured-bg.jpg') }})">
		<div class="marketing">
			<div class="tagline">
				<p>Help us out and complete our surveys</p>
			</div>
		</div>
	</header>

	<div class="introduction">
		<div class="purpose">
			<h1>Abertay Attribute Survey Project</h1>
				<p>This survey involves answering questions regarding how current Abertay students view themselves and their own skills. Based on your answers, we want to explore how we can support students in developing the Abertay attributes that the University envisages all students to develop throughout the course of their study.</p>
				<p>The survey will ask you for some demographic information initially. It will then give you some statements and you are asked to which extent you agree or disagree with those statements on a 7-point scale. The survey takes about 10-15 minutes to complete.</p>
				<p>Your participation is completely voluntary and your responses will be kept confidential to the researchers involved in this study. Potential publications arising from your responses will be anonymous, which means we will not refer to your identity in any reports. You are not obliged to take part in the survey and you are free to withdraw from the survey at any time.</p>
				<p>By starting the survey you indicate that you understand the conditions of the study as described above and provide informed consent to participate in this study.
				<p>In case of any questions you can contact Dr Mulholland <a href="mailto:g.mulholland@abertay.ac.uk">(g.mulholland@abertay.ac.uk)</a>, Mr  Simpson <a href="mailto:e.simpson@abertay.ac.uk">(e.simpson@abertay.ac.uk)</a>, Dr Smith <a href="kate.smith@abertay.ac.uk">(kate.smith@abertay.ac.uk)</a> or Dr Szymkowiak <a href="mailto:a.szymkowiak@abertay.ac.uk">(a.szymkowiak@abertay.ac.uk).</p>
			<hr>
		</div>
	</div>
</div>
@endsection


