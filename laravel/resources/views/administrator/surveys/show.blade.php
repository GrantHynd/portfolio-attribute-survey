@extends('layout')
@section('content')
<div class="main-content">
    <div class="callout secondary">
        <h6>Instructions</h6>
        <p>
            A survey can be run multiple times, each instance of a survey is referred to as a survey instance. Simply create a survey instance by selecting a start and end date (time is automatically set to 12:00am).<br>
            <a href="#schedualed-instances">Create Survey Instance</a>
        </p>
        <p>Once a survey instance receives responses, an Excel file can be downloaded which can be filtered by school.</p>
        <p>There are 3 states of a survey instances and they are displayed on this page:
        <ul>
            <li><a href="#active-instances">View Active Instances</a> - students can view and complete, admins can download responses.</li>
            <li><a href="#finished-instances">View Finished Instances</a> - students can no longer view or complete, admins can download responses.</li>
            <li><a href="#schedualed-instances">View Scheduled Instances</a> - students cannot view or complete until start date.</li>
        </ul>
    </div>
    <div class="survey-intro">
        <div class="survey-info">
        <div class="title"><h1>{{ $survey->name }}</h1></div>
        <pre>{{ $survey->desc }}</pre>
        <p>{{ count($survey->instances()->ended()->get()) }} previous instances  |
        {{ count($survey->instances()->active()->get()) }} active instances  |
        {{ count($survey->instances()->scheduled()->get()) }} scheduled instances</p>
        </div>
        <div class="survey-button">
            <a title="Edit Survey" class="red-button" href="{{ url('/administrator/surveys/'.$survey->id.'/edit') }}">
                <span class="fa fa-pencil fa-2x"></span>
            </a>
            <a title="Duplicate Survey" class="red-button" href="{{ url('/administrator/surveys/'.$survey->id.'/duplicate') }}">
                <span class="fa fa-copy fa-2x"></span>
            </a>
            <form title="Remove" class="button-form" action="{{ url('/administrator/surveys/'.$survey->id) }}" @submit.prevent="onRemove">
                <button class="red-button"><span class="fa fa-trash fa-2x"></span></button>
            </form>
        </div>
    </div>

    @if(count($survey->instances()->active()->get()) !== 0)
        <div class="instance-list-section">
            <h3 class="title"><a name="active-instances">Active Survey Instances</a></h3>
            @foreach($survey->instances()->active()->get() as $instance)
                <div class="instance">
                    <div class="instance-heading">
                        <a href="{{ url('/surveys/'.$survey->id.'/instances/'.$instance->id) }}"><h3 class="instance-title">#{{ $instance->id }}</h3></a>
                        <div class="instance-button-remove">
                            <form action="{{ url('/administrator/surveys/'.$survey->id.'/instances/'.$instance->id) }}" @submit.prevent="onRemove">
                                <button class="red-button"><span class="fa fa-trash"></span></button>
                            </form>
                        </div>
                    </div>
                    <div class="instance-startdate"><p>Starts on: <b>{{ $instance->start_at->toFormattedDateString() }}</b></p></div>
                    <div class="instance-enddate"><p>Ends on: <b>{{ $instance->end_at->toFormattedDateString() }}</b></p></div>
                    <div class="instance-responses">
                        <p>{{ $instance->questions()->distinct('user_id')->count('user_id') }} responses</p>
                    </div>
                    <div class="instance-footer">
                        <p>Download options:</p>
                        <a class="red-button instance-button-download" href="{{ url('/administrator/surveys/'.$survey->id.'/instances/'.$instance->id.'/download') }}">All</a>
                        <a class="red-button instance-button-download" href="{{ url('/administrator/surveys/'.$survey->id.'/instances/'.$instance->id.'/download/graduate-school') }}">Graduate</a>
                        <a class="red-button instance-button-download" href="{{ url('/administrator/surveys/'.$survey->id.'/instances/'.$instance->id.'/download/DBS') }}">DBS </a>
                        <a class="red-button instance-button-download" href="{{ url('/administrator/surveys/'.$survey->id.'/instances/'.$instance->id.'/download/SDI') }}">SDI</a>
                        <a class="red-button instance-button-download" href="{{ url('/administrator/surveys/'.$survey->id.'/instances/'.$instance->id.'/download/SET') }}">SET</a>
                        <a class="red-button instance-button-download" href="{{ url('/administrator/surveys/'.$survey->id.'/instances/'.$instance->id.'/download/SHS') }}">SHS</a>
                    </div>
                </div>
            @endforeach
        </div>
        <hr>
    @endif

    @if(count($survey->instances()->ended()->get()) !== 0)
    <div class="instance-list-section">
        <h3 class="title"><a name="finished-instances">Finished Survey Instances</a></h3>
        @foreach($survey->instances()->ended()->get() as $instance)
            <div class="instance">
                <div class="instance-heading">
                    <a href="{{ url('/surveys/'.$survey->id.'/instances/'.$instance->id) }}"><h3 class="instance-title">#{{ $instance->id }}</h3></a>
                    <div class="instance-button-remove">
                        <form action="{{ url('/administrator/surveys/'.$survey->id.'/instances/'.$instance->id) }}" @submit.prevent="onRemove">
                            <button class="red-button"><span class="fa fa-trash"></span></button>
                        </form>
                    </div>
                </div>
                <div class="instance-startdate"><p>Starts on: <b>{{ $instance->start_at->toFormattedDateString() }}</b></p></div>
                <div class="instance-enddate"><p>Ends on: <b>{{ $instance->end_at->toFormattedDateString() }}</b></p></div>
                <div class="instance-responses">
                    <p>{{ $instance->questions()->distinct('user_id')->count('user_id') }} responses</p>
                </div>
                <div class="instance-footer">
                    <p>Download options:</p>
                    <a class="red-button instance-button-download" href="{{ url('/administrator/surveys/'.$survey->id.'/instances/'.$instance->id.'/download') }}">All</a>
                    <a class="red-button instance-button-download" href="{{ url('/administrator/surveys/'.$survey->id.'/instances/'.$instance->id.'/download/graduate-school') }}">Graduate</a>
                    <a class="red-button instance-button-download" href="{{ url('/administrator/surveys/'.$survey->id.'/instances/'.$instance->id.'/download/DBS') }}">DBS </a>
                    <a class="red-button instance-button-download" href="{{ url('/administrator/surveys/'.$survey->id.'/instances/'.$instance->id.'/download/SDI') }}">SDI</a>
                    <a class="red-button instance-button-download" href="{{ url('/administrator/surveys/'.$survey->id.'/instances/'.$instance->id.'/download/SET') }}">SET</a>
                    <a class="red-button instance-button-download" href="{{ url('/administrator/surveys/'.$survey->id.'/instances/'.$instance->id.'/download/SHS') }}">SHS</a>
                </div>
            </div>
        @endforeach
    </div>
    <hr>
    @endif

    <div class="instance-list-section">
        <h3 class="title"><a name="schedualed-instances">Scheduled Survey Instances</a></h3>
        @foreach($survey->instances()->scheduled()->get() as $instance)
            <div class="instance">
                <div class="instance-heading">
                    <a href="{{ url('/surveys/'.$survey->id.'/instances/'.$instance->id) }}"><h3 class="instance-title">#{{ $instance->id }}</h3></a>
                    <div class="instance-button-remove">
                        <form action="{{ url('/administrator/surveys/'.$survey->id.'/instances/'.$instance->id) }}" @submit.prevent="onRemove">
                            <button class="red-button"><span class="fa fa-trash"></span></button>
                        </form>
                    </div>
                </div>
                <div class="instance-startdate"><p>Starts on: <b>{{ $instance->start_at->toFormattedDateString() }}</b></p></div>
                <div class="instance-enddate"><p>Ends on: <b>{{ $instance->end_at->toFormattedDateString() }}</b></p></div>
            </div>
        @endforeach
        <div class="instance">
            <div class="instance-heading">
                <h3 class="instance-title">New Instance</h3>
            </div>
            <form class="form" method="POST" action="{{ url('/administrator/surveys/'.$survey->id.'/instances') }}">
                {{ csrf_field() }}
                <div class="form-instance-container">
                    <div class="form-item form-instance-startdate">
                        <label>From: </label>
                        <input type="date" placeholder="Select start date" name="start_at"/>
                    </div>

                    <div class="form-item form-instance-enddate">
                        <label>Until: </label>
                        <input type="date" placeholder="Select end date" name="end_at"/>
                    </div>
                    <div class="form-item form-instance-footer">
                        <button type="submit" class="red-button">Create</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection