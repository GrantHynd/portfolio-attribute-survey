@extends('layout')

@section('content')
<div class="main-content">
  <div class="callout secondary">
    <h6>Instructions</h6>
    <p>A survey refers to the list of questions. Survey contains mutiple instances which can be viewed (eye icon), surveys can also be edited (pencil icon) and duplicated (files icon). Finally surveys can be deleted (trash icon), this can not be undone.</p>
  </div>
  <div class="survey-list-section">
  <div style="position: absolute; right: 0px;">
    <a class="red-button" href="{{ url('/administrator/surveys/create') }}">
      <span class="fa fa-plus"> Create Survey</span>
    </a>
  </div>
  <h1>Surveys</h1>
    @if(count($surveys) == 0)
    <div class="survey-intro">
      <div class="survey-info">
        <p>No surveys exist, <a href="{{ url('/administrator/surveys/create') }}">create a survey</a> to get started.</p>
      </div>
    </div>
    @else
      @foreach($surveys as $survey)
        <div class="survey-intro">
          <div class="survey-info">
            <div class="title"><h3><a href="{{ url('/administrator/surveys/'.$survey->id) }}">{{ $survey->name }}</a></h3></div>
            <pre>{{ $survey->desc }}</pre>
            <p>{{ count($survey->instances()->ended()->get()) }} previous instances  |
            {{ count($survey->instances()->active()->get()) }} active instances  |
            {{ count($survey->instances()->scheduled()->get()) }} scheduled instances</p>
          </div>
          <div class="survey-button">
            <a title="View Instances" class="red-button" href="{{ url('/administrator/surveys/'.$survey->id) }}">
              <span class="fa fa-eye fa-2x"></span>
            </a>
            <a title="Edit Survey" class="red-button" href="{{ url('/administrator/surveys/'.$survey->id.'/edit') }}">
              <span class="fa fa-pencil fa-2x"></span>
            </a>
            <a title="Duplicate Survey" class="red-button" href="{{ url('/administrator/surveys/'.$survey->id.'/duplicate') }}">
                <span class="fa fa-copy fa-2x"></span>
            </a>
            <form class="button-form" action="{{ url('/administrator/surveys/'.$survey->id) }}" @submit.prevent="onRemove">
              <button title="Remove" class="red-button">
                <span class="fa fa-trash fa-2x"></span>
              </button>
            </form>
          </div>
        </div>
      @endforeach
    @endif
  </div>
</div>
@endsection