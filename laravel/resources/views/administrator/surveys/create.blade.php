@extends('layout')
@section('content')
<div class="main-content">
    <div class="callout secondary">
        <p>A survey can contain multiple sections, each section may contain multiple questions</p>
        <p>Questions may be added and edited but the order cannot be rearranged. Plan the order of questions before survey creation.</p>
    </div>
    <create-survey></create-survey>
</div>
@endsection