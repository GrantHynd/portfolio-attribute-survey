@extends('layout')

@section('content')
<div class="main-content">
    <div class="callout secondary">
        <h5>Warning</h5>
        <p>If a survey has survey instances that are active or finished then editing the survey will invalidate previous responses.</p>
        <p>The safest option is to only edit surveys that have 0 instances. If a survey has instances but changes are required then duplicate the survey and edit the duplicated copy.</p>
    </div>
    <edit-survey :survey-id="{{ $survey->id }}"></edit-survey>
</div>
@endsection