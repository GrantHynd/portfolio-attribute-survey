@extends('layout')

@section('content')
    <div class="main-content">
        <form method="POST" action="{{ url('/sessions/store') }}">
            {{ csrf_field() }}
            <button type="submit" class="button">Authenticate using university account</button>
        </form>
    </div>
@endsection