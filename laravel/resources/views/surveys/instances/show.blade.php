@extends('layout')

@section('content')
<div class="main-content">
    <div class="survey-intro">
        <div class="survey-info">
        <div class="title"><h1>{{ $survey->name }}</h1></div>
        <pre>{{ $survey->desc }}</pre>
        <p>Active from <b>{{ $instance->start_at->toFormattedDateString()  }}</b>  until  <b>{{ $instance->end_at->toFormattedDateString() }}</b></p>
        @if(Auth::check() && Auth::user()->isAdmin())
            <p>{{ $numberOfResponses }} <a href="{{ url('/administrator/surveys/'.$survey->id) }}">responses</a></p>
        @endif
        </div>
    </div>

    @if(Auth::check())
        @if($instance->isActive() && !Auth::user()->hasCompletedInstance($instance->id))
            <survey :survey-id="{{ $survey->id }}" :instance-id="{{ $instance->id }}"></survey>

        @elseif(Auth::user()->hasCompletedInstance($instance->id))  
            <div class="page-error-message">
                <p>You have already completed this survey. Thank you for your assistance.</p>
            </div>

        @elseif(!$instance->isActive())  
            <div class="page-error-message">
                <p>This survey is not currently active.</p>
            </div>

        @endif
    @endif
</div>
@endsection
