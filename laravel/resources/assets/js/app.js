
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
$(document).ready(function(){
    $(document).foundation();
});

window.Vue = require('vue');

import "babel-polyfill";

import CreateSurvey from './components/surveys/CreateSurvey.vue';
import EditSurvey from './components/surveys/EditSurvey.vue';
import Survey from './components/surveys/Survey.vue';
import swal from 'sweetalert2';

new Vue({
    el: '#app',

    components: { CreateSurvey, EditSurvey, Survey },

    methods: {
        onRemove(event) {
            var form = event.srcElement;
            var action = form.action;

            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(() => {
                this.deleteObjectFromDatabase(action);
            }).catch((error) => {
                console.log(error);
            });
        },

        deleteObjectFromDatabase(action) {
            axios.delete(action)
            .then(url => {
                swal({
                    title: 'Deleted!',
                    text: 'The item has been deleted.',
                    type: 'success',
                    timer: 4000
                }).then(() => {
                    window.location.href = url.data;
                },
                (dismiss) => {
                    window.location.href = url.data;
                });
            });
        }
    }
})