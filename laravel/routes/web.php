<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Pages */
Route::get('/', 'PageController@home')->name('home');

/* Sessions */
Route::get('/login', 'SessionController@create')->name('login');
Route::post('/sessions/store', 'SessionController@store');
Route::get('/logout', 'SessionController@destroy');

/* Frontend user area */
Route::group(['middleware' => 'auth'], function () {
    Route::get('/surveys/{survey}/instances/{instance}', 'InstanceController@show');
    Route::post('/surveys/{survey}/instances/{instance}/response', 'ResponseController@store');
});

/* Backend admin area */
Route::group(['middleware' => 'admin', 'prefix' => 'administrator'], function () {

    /* Survey Instances */
    Route::get('/surveys/{survey}/instances', 'InstanceController@adminIndex');
    Route::post('/surveys/{survey}/instances', 'InstanceController@store');
    Route::get('/surveys/{survey}/instances/{instance}/edit', 'InstanceController@edit');
    Route::patch('/surveys/{survey}/instances/{instance}', 'InstanceController@update');
    Route::delete('/surveys/{survey}/instances/{instance}', 'InstanceController@destroy');
    
    /* Surveys */
    Route::get('/surveys', 'SurveyController@adminIndex');
    Route::get('/surveys/create', 'SurveyController@create');
    Route::post('/surveys', 'SurveyController@store');
    Route::get('/surveys/{survey}', 'SurveyController@adminShow');
    Route::get('/surveys/{survey}/edit', 'SurveyController@edit');
    Route::patch('/surveys/{survey}', 'SurveyController@update');
    Route::get('/surveys/{survey}/duplicate', 'SurveyController@duplicate');
    Route::delete('/surveys/{survey}', 'SurveyController@destroy');

    /* Sections */
    Route::delete('/sections/{section}', 'SectionController@destroy');
    
    /* Questions */
    Route::delete('/questions/{question}', 'QuestionController@destroy');

    /* Question Options */
    Route::delete('/question-options/{option}', 'QuestionOptionController@destroy');

    /* Responses */
    Route::get('/surveys/{survey}/instances/{instance}/download/{school?}', 'ResponseController@download');
});

/* API calls */
Route::group(['prefix' => 'api'], function () {
    Route::get('/question-types', 'ApiController@getQuestionTypes');
    Route::get('/surveys/{survey}/instances/{instance}', 'ApiController@getSurveyInstance');
    Route::get('/surveys/{survey}/sections/{section}/questions', 'ApiController@getSectionQuestions');
    Route::get('/surveys/{survey}', 'ApiController@getSurvey');
    Route::get('/surveys/{survey}/sections', 'ApiController@getSections');
});