<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SurveysTest extends TestCase
{
    /** @test */
    public function a_user_can_browse_surveys()
    {
        $response = $this->get('/surveys');

        $response->assertStatus(200);
    }
}
